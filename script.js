//falta botar o feature de travar o tabuleiro para observar no final de uma rodada e liberar a proxima com um botao

function carregarTabuleiro() {

	ResetarBotoesBottom();

	var winsP2 = 0;
	var winsP1 = 0;
	var empates = 0;
	var tabuleiro = new Array(3);
	var cont_mat = 0;
	var player1 = "Luigi";
	var player2 = "Mario";

	var janela = document.getElementsByClassName("rcorners1");
	var jmaindisplay = document.getElementById("table1");
	for(var i=0; i<3; i++) {
	  tabuleiro[i] = new Array(3);
	  for(var j=0; j<3; j++) {
	    tabuleiro[i][j] = janela[cont_mat];
	    cont_mat++;
	  }
	}

	var janelaaux = document.getElementsByClassName("rcornersaux");
	var jauxdisplay = document.getElementById("table2");

	jauxdisplay.style.display = "none";

	function HabilitarJanela(item){
		item.addEventListener("mouseover", function() { MouseOver(item) }, true);
		item.addEventListener("mouseout", function() { MouseOut(item) }, true);
		item.addEventListener("mousedown", function() { MouseDown(item) }, true);
		item.addEventListener("mouseup", function() { MouseUp(item) }, true);
		item.addEventListener("click", function() { Click(item)}, true);
		item.addEventListener("click", function(){ ChecarVitoria() }, true)
	}
	for (var i = 0; i < janela.length; i++) {
		let item = janela[i];
		HabilitarJanela(item);
	}

	document.getElementById("but1").addEventListener("click",function(){InputNome1()});
	document.getElementById("but2").addEventListener("click",function(){InputNome2()});
	document.getElementById("but3").addEventListener("click",function(){ResetarPontuacao()});
	
	function InputNome1(){
		player1 = document.getElementById("input1").value;
		document.getElementById("pontLu").innerHTML = player1;
	}

	function InputNome2(){
		player2 = document.getElementById("input2").value;
		document.getElementById("pontMa").innerHTML = player2;
	}

	function ResetarPontuacao() {
		winsP2 = 0;
		winsP1 = 0;
		document.getElementById("pontuacao1").textContent = winsP1;
		document.getElementById("pontuacao2").textContent = winsP2;
		AtualizarStatus("Pontuação Resetada","rgb(255,255,255,0.5)");
		ZerarTabuleiro();
		jmaindisplay.style.display = "block";
		jauxdisplay.style.display = "none";
		ResetarBotoesBottom();
	}

	function MouseOver(item) {
  	item.style.background = "rgb(255,255,255,1.0)";
	}

	function MouseOut(item) {
  	item.style.background = "rgb(255,255,255,0.7)";
	}

	function MouseDown(item){
		item.style.background = "rgb(0,255,255,1.0)";
	}

	function MouseUp(item){
		item.style.background = "rgb(255,255,255,1.0)";
	}

	var xouo = "X";

	function AtualizarStatus(string, corback){
		status.textContent = string;
		document.getElementById("statusback").style.background = corback;
	}

	function InvertXouO(){
		if (xouo == "X"){
			xouo = "O";
			AtualizarStatus("Vez de " + player2 +"!","lightcoral");
			document.getElementById("colMario").style.background = "yellow";
			document.getElementById("colLuigi").style.background = "rgb(255,255,255,0.3)";
		} else if (xouo == "O"){
			xouo = "X";
			AtualizarStatus("Vez de " + player1 +"!","lime");
			document.getElementById("colLuigi").style.background = "yellow";
			document.getElementById("colMario").style.background = "rgb(255,255,255,0.3)";
		}
	}

	function Click(item){
		if (item.textContent == ""){
			item.textContent = xouo;
			InvertXouO();
		}
	}

	var status = document.getElementById("status");

	function StatusVitoria(referencia){
		if (referencia.textContent === "X"){
			AtualizarStatus(player1 + " ganhou!","cyan");
			winsP1++;
			document.getElementById("pontuacao1").textContent = winsP1;
		} else{
			AtualizarStatus(player2 + " ganhou!","cyan");
			winsP2++;
			document.getElementById("pontuacao2").textContent = winsP2;
		}
	}

	function IgualdadeGrid(a,b,c){
		if(a.textContent == b.textContent && b.textContent == c.textContent && a.textContent != ""){
			return true;
		}
	}

	function ChecarVitoria(){
		
		for(var i = 0; i < 3; i++){
			if( IgualdadeGrid(tabuleiro[i][0],tabuleiro[i][1],tabuleiro[i][2]) ){
				StatusVitoria(tabuleiro[i][0]);
				ZerarTabuleiro();
				MudarBotoesBottom();
			} else if( IgualdadeGrid(tabuleiro[0][i],tabuleiro[1][i],tabuleiro[2][i]) ){
				StatusVitoria(tabuleiro[0][i]);
				ZerarTabuleiro();
				MudarBotoesBottom();
			}	else if( IgualdadeGrid(tabuleiro[0][0],tabuleiro[1][1],tabuleiro[2][2]) ){
				StatusVitoria(tabuleiro[0][0]);
				ZerarTabuleiro();
				MudarBotoesBottom();
			} else if( IgualdadeGrid(tabuleiro[2][0],tabuleiro[1][1],tabuleiro[0][2]) ){
				StatusVitoria(tabuleiro[1][1]);
				ZerarTabuleiro();
				MudarBotoesBottom()
			}
		}	
		var cont2 = 0;
		for(var i = 0; i < janela.length; i++){
			if(janela[i].textContent != ""){
				cont2++;
			}
		}
		if (cont2 == 9){
			AtualizarStatus("EMPATE!","cyan");
			empates++;
			ZerarTabuleiro();
			MudarBotoesBottom();
		}
	}

	function ZerarTabuleiro(){

		jmaindisplay.style.display = "none";

		for (var i = 0; i < janela.length; i++){
		janelaaux[i].textContent = janela[i].textContent;
		}		

		jauxdisplay.style.display = "block";

		for (var i = 0; i < janela.length; i++){
			janela[i].textContent = "";
		}
		cont = 0;
	}

	function ProximaRodada(){
		jmaindisplay.style.display = "block";
		jauxdisplay.style.display = "none";
		AtualizarStatus("Rodada " + (1+winsP2+winsP1+empates),"orange");	
	}

	function ResetarBotoesBottom(){
		document.getElementById("but4").style.display = "none";
		document.getElementById("classB3").className += " offset-4";
	}

	function MudarBotoesBottom(){
		document.getElementById("but4").style.display = "block";
		document.getElementById("classB3").className = "col-4";		
	}

	document.getElementById("but4").addEventListener("click", function() {ProximaRodada(); ResetarBotoesBottom()});

}
window.onload = carregarTabuleiro;